#######################################
# *********************************** #
# * A primitive stock investigation * #
# *********************************** #
#######################################



library(quantmod)

# Download stock data into xts objects:
ibm      <- getSymbols("IBM",   src="yahoo", startdate="2012-01-01");
google   <- getSymbols("GOOGL", src="yahoo");
facebook <- getSymbols("FB",    src="yahoo");
alibaba  <- getSymbols("BABA",  src="yahoo");

# Plot the EOD graphs:
#plot(IBM$IBM.Close)
#plot(GOOGL$GOOGL.Close);
#plot(FB$FB.Close);
#plot(BABA$BABA.Close);

# Find out the age in days of the youngest stock:
stock.table.length <- min(dim(IBM)[1],
                          dim(GOOGL)[1],
                          dim(FB)[1],
                          dim(BABA)[1]);

# Function to extract EOD data from xts objects:
extract.data <- function(data.column, tail.length)
{
  drop(coredata(data.column[seq(length(data.column) - tail.length + 1,
                                length(data.column))]))
}

# Build a table of EOD data from downloaded stock data:
stock.eod.table <- rbind(seq(-stock.table.length,-1),
                         extract.data(IBM$IBM.Close,     stock.table.length),
                         extract.data(GOOGL$GOOGL.Close, stock.table.length),
                         extract.data(FB$FB.Close,       stock.table.length),
                         extract.data(BABA$BABA.Close,   stock.table.length));



# Full-Single-Prediction:
# =======================


# The first predictor will be the solution of a linear n x n system (thus "full").
# It will be fed by stock prices of the same stock only (thus "single").
# This predictor turns out to create huge deviations from the last price and
# hence seems to be very bad.


# Function to build a matrix (A) of a linear system A * x = b,
# where the solution x is supposed to serve as a predictor.
# Let t be a stock.table, k the stock to be predicted and n the number of
# considered stock prices of type k:
matr.full.single <- function(t,n,k)
{
  l <- dim(t)[2];
  
  if (l < 2 * n)
    cat ("Error: n =", n, " is too large for this table!");

  i <- l - 2 * n + 1;
  
  A <- t[k, seq(i, i + n - 1)];
  if (i + 1 <= i + n - 1)
  {
    for (j in seq(i + 1, i + n - 1))
    {
      A <- rbind(A, t[k, seq(j, j + n - 1)]);
    }
  }

  A
}


# Function to build a vector (b) of a linear system A * x = b, 
# where the solution x is supposed to serve as a predictor.
# Let t be a stock.table, k the stock to be predicted and n the number of
# considered stock prices of type k:
vec.full.single <- function(t,n,k)
{
  l <- dim(t)[2];
  
  if (l < 2 * n)
    cat ("Error: n =", n, " is too large for this table!");
  
  t[k, seq(l - n + 1, l)]  
}


# Function to calculate the Full-Single-Predictor of stock k given
# stock.table t and number of stock prices to be considered n:
pred.full.single <- function(t,n,k)
{
  A <- matr.full.single(t, n, k);
  b <- vec.full.single(t, n, k);
  x <- solve(A,b);

  x
}


# Function to calculate the Full-Single-Prediction of stock k given
# stock.table t and number of stock prices to be considered n:
full.single.prediction <- function(t,n,k)
{
  A <- matr.full.single(t, n, k);
  b <- vec.full.single(t, n, k);
  x <- solve(A,b);
  p <- crossprod(b,x);
  
  p
}


# Function to plot the prediction into the chart:
plot.full.single.prediction <- function(t, n, k, headline)
{
  p <- full.single.prediction(t,n,k);
  
  y.lim <- c(min(t[k,], p),
             max(t[k,], p));
  
  plot(t[k,],
       ylim = y.lim,
       type = 'l', col = "blue", main = headline,
       xlab = "Entry of Price-Table", ylab = "Stock Price");
  points(dim(t)[2] + 1, p, col = "red");
}


# Example 1:
#-----------

# Creating a stock.table with a longer width (namely this time without Alibaba):
# Find out the age in days of the youngest stock:
stock.table.length2 <- min(dim(IBM)[1],
                           dim(GOOGL)[1],
                           dim(FB)[1]);

# Build a table of EOD data from downloaded stock data:
stock.eod.table2 <- rbind(seq(-stock.table.length2,-1),
                          extract.data(IBM$IBM.Close, stock.table.length2),
                          extract.data(GOOGL$GOOGL.Close, stock.table.length2),
                          extract.data(FB$FB.Close, stock.table.length2));

# Find out the maximum length to consider for this predictor:
l2 <- floor(stock.table.length2 / 2);

# To find out the prediction for IBM for example, inspect this value:
full.single.prediction(stock.eod.table2, l2, 2); # 2 is the line for IBM

# To test, if the predictor is really correct:
test.day <- -4; # For example to test against the 4th-latest data, inspect these:
crossprod(pred.full.single(stock.eod.table2, l2, 2),
          stock.eod.table2[2, seq(stock.table.length2 + test.day + 1 - l2,
                                  stock.table.length2 + test.day)]);
stock.eod.table2[2, stock.table.length2 + test.day + 1];

# To view the predictions in the charts:
plot.full.single.prediction(stock.eod.table2, l2, 2, "IBM F-S-Prediction");
plot.full.single.prediction(stock.eod.table2, l2, 3, "Google F-S-Prediction");
plot.full.single.prediction(stock.eod.table2, l2, 4, "Facebook F-S-Prediction");


# Example 2:
#-----------

# Creating a stock.table:
# Find out the age in days of the youngest stock:
stock.table.length3 <- min(dim(IBM)[1],
                           dim(GOOGL)[1],
                           dim(FB)[1]);

# Build a table of EOD data from downloaded stock data:
stock.eod.table3 <- rbind(seq(-stock.table.length3,-1),
                          extract.data(IBM$IBM.Close,     stock.table.length3),
                          extract.data(GOOGL$GOOGL.Close, stock.table.length3),
                          extract.data(FB$FB.Close,       stock.table.length3));

# Calculating predictions day by day:
predictions <- rep(-1, stock.table.length3);
for (d in 2:stock.table.length3)
{
  t <- stock.eod.table3[, 1:d]; # This could be faster!
  predictions[d] <- full.single.prediction(t, floor(d / 2), 4);
}
lines( c(c(-1), predictions), col = "red");
points(c(c(-1), predictions), col = "darkgreen", pch = "x");
# The last 2 commands require Example 1 to be executed first.

