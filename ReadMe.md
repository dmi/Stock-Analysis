Project Goals
-------------

In this script, I tried to predict stock prices by modeling each day's price as a linear combination of its preceeding EOD-prices.
A further idea was to take data of other stocks of the same industry or global economy into account as well, but the bad results of the first goal made me pause at this point.



Requirements
------------

The R-package "quantmod" is required.



Results
-------

The result is obviously a massively overfitted model, which I did not know of at the time of writing.
The first improvement would probably be a reduction of features and a data splitting into training- and validation-data.
